package llog

import (
	"testing"
	
	ee "gitlab.com/nikonor/eerrors"
)

func BenchmarkLL1(b *testing.B) {
	Start(3)
	StartStdoutStream()
	
	for i := 0; i < b.N; i++ {
		e := ee.Debug().WithMessage("qwe")
		Log(e)
	}
}
func BenchmarkLL2(b *testing.B) {
	Start(3)
	StartStdoutStream()
	
	var e *ee.Message
	for i := 0; i < b.N; i++ {
		e = ee.Debug().WithMessage("qwe")
		Log(e)
	}
}

func BenchmarkLLWO(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e := ee.Debug().WithMessage("qwe")
		e.MaskWords([]string{"qwe"})
		e = nil
	}
}
