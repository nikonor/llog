package llog

import (
	"errors"
	"fmt"
	"log/syslog"
	"os"
	"strings"
	"sync"
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
)

// LogFunc - тип функций для логирования
type LogFunc func(mes *ee.Message)
type FilterFunc func(in error) *ee.Message

var (
	streamCount                                          int
	debug                                                bool
	logChannel                                           chan *ee.Message
	doneChan                                             chan struct{}
	lock                                                 sync.RWMutex
	currentLogLevel                                      int
	funcs                                                []LogFunc
	outFName, errFName                                   string
	outFH                                                *os.File
	errFH                                                *os.File
	sysLogStreamID, stdoutStreamID, oStreamID, eStreamID int
	filterFunc                                           FilterFunc
)

func init() {
	// funcs = make(map[string]LogFunc)
}

func DebugToggle(needLock ...bool) {
	if len(needLock) == 0 || needLock[0] {
		lock.Lock()
		defer lock.Unlock()
	}
	debug = !debug
}

func d(v ...interface{}) {
	if len(v) > 0 {
		if debug {
			fmt.Print(" ", v, "\n")
		}
	}
}

func ReOpenFiles(needLock ...bool) error {
	d("Вызов ReOpenFiles")
	defer d("Окончание ReOpenFiles")
	var err error
	if len(needLock) == 0 || needLock[0] {
		lock.Lock()
		defer lock.Unlock()
	}
	
	if outFH != nil {
		outFH.Close()
		outFH, err = os.OpenFile(outFName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
		if err != nil {
			return err
		}
	}
	if errFH != nil {
		errFH.Close()
		errFH, err = os.OpenFile(errFName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
		if err != nil {
			return err
		}
	}
	return nil
}

func CloseOutFile(needLock ...bool) error {
	d("Вызов CloseOutFile")
	defer d("Окончание CloseOutFile")
	if len(needLock) == 0 || needLock[0] {
		lock.Lock()
		defer lock.Unlock()
	}
	return outFH.Close()
}

func CloseErrFile(needLock ...bool) error {
	d("Вызов CloseErrFile")
	defer d("Окончание CloseErrFile")
	if len(needLock) == 0 || needLock[0] {
		lock.Lock()
		defer lock.Unlock()
	}
	return errFH.Close()
}

func RemoveOutFile(needLock ...bool) error {
	d("Вызов RemoveOutFile")
	defer d("Окончание RemoveOutFile")
	if outFH != nil {
		if len(needLock) == 0 || needLock[0] {
			lock.Lock()
			defer lock.Unlock()
		}
		RemoveStream(oStreamID)
		// outFH.Sync()
		outFH.Close()
	}
	return nil
}

func AddOutFile(fname string, needLock ...bool) error {
	d("Вызов AddOutFile")
	defer d("Окончание AddOutFile")
	var err error
	if len(needLock) == 0 || needLock[0] {
		lock.Lock()
		defer lock.Unlock()
	}
	outFName = fname
	if outFH, err = os.OpenFile(fname, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660); err != nil {
		return err
	}
	oStreamID, err = AddStream(ofile, false)
	return err
}

func RemoveErrFile(needLock ...bool) error {
	d("Вызов RemoveErrFile")
	defer d("Окончание RemoveErrFile")
	if errFH != nil {
		if len(needLock) == 0 || needLock[0] {
			lock.Lock()
			defer lock.Unlock()
		}
		RemoveStream(eStreamID, false)
		// errFH.Sync()
		errFH.Close()
	}
	return nil
}

func AddErrFile(fname string, needLock ...bool) error {
	d("Вызов AddErrFile")
	defer d("Окончание AddErrFile")
	var err error
	if len(needLock) == 0 || needLock[0] {
		lock.Lock()
		defer lock.Unlock()
	}
	
	errFName = fname
	if errFH, err = os.OpenFile(fname, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660); err != nil {
		return err
	}
	eStreamID, err = AddStream(efile, false)
	return err
}

// StartStdoutStream - вывод лога в консоль
func StartStdoutStream() int {
	d("Вызов StartStdoutStream")
	defer d("Окончание StartStdoutStream")
	var err error
	stdoutStreamID, err = AddStream(stdout)
	if err != nil {
		return -1
	}
	return stdoutStreamID
}

// StopStdoutStream - остановка записи в stdout
func StopStdoutStream() {
	d("Вызов StopStdoutStream")
	defer d("Окончание StopStdoutStream")
	RemoveStream(stdoutStreamID)
	return
}

// StartSyslogStream - добавляем работу с syslog
func StartSyslogStream() {
	d("Вызов StartSyslogStream")
	defer d("Окончание StartSyslogStream")
	var (
		err error
	)
	p := strings.Split(os.Args[0], "/")
	binName := p[len(p)-1]
	
	syslogger, err = syslog.New(syslog.Priority(currentLogLevel), binName)
	if err != nil {
		panic(err)
	}
	
	sysLogStreamID, _ = AddStream(writeToSysLog)
}

// StopSyslogStream - остановка логгирования в syslog
func StopSyslogStream() {
	d("Вызов StopSyslogStream")
	defer d("Окончание StopSyslogStream")
	RemoveStream(sysLogStreamID)
	return
}

// AddStream - добавляем поток для вывода логов
func AddStream(fn func(mes *ee.Message), needLock ...bool) (int, error) {
	d("Вызов AddStream")
	defer d("Окончание AddStream")
	if len(needLock) == 0 || needLock[0] {
		lock.Lock()
		defer lock.Unlock()
	}
	funcs = append(funcs, fn)
	streamCount++
	return streamCount - 1, nil
}

func RemoveStream(id int, needLock ...bool) {
	d("Вызов RemoveStream")
	defer d("Окончание RemoveStream")
	if len(needLock) == 0 || needLock[0] {
		lock.Lock()
		defer lock.Unlock()
	}
	if id > len(funcs)-1 {
		return
	}
	funcs = append(funcs[0:id], funcs[id+1:]...)
}

// GetLogLevel - возвращаем текущий LogLevel
func GetLogLevel() int {
	lock.RLock()
	defer lock.RUnlock()
	return currentLogLevel
}

// SetLogLevel - установка уровня логирования
//  notNeedLock - принимает один параметр. Если он выставлен в true, то мы не лочим мьютекс
func SetLogLevel(lev int, notNeedLock ...bool) {
	d("Вызов SetLogLevel")
	defer d("Окончание SetLogLevel")
	// if len(notNeedLock) == 0 || !notNeedLock[0] {
	lock.Lock()
	defer lock.Unlock()
	// }
	currentLogLevel = lev
}

// Stop - завершение работы
func Stop() {
	close(doneChan)
	funcs = []LogFunc{}
	time.Sleep(time.Second)
}

// Start - начало работы модуля логирования
func Start(logLevel int) (chan *ee.Message, chan struct{}, error) {
	d("Вызов Start")
	defer d("Окончание Start")
	lock.RLock()
	if currentLogLevel != 0 {
		lock.RUnlock()
		return nil, nil, errors.New("модуль уже стартовал")
	}
	lock.RUnlock()
	
	SetLogLevel(logLevel, true)
	logChannel = make(chan *ee.Message)
	doneChan = make(chan struct{})
	
	// if err := AddStream("STDERR", 3, stderr); err != nil {
	// 	return nil, nil, err
	// }
	
	go func() {
		var mes *ee.Message
		for {
			select {
			case <-doneChan:
				lock.Lock()
				currentLogLevel = 0
				lock.Unlock()
				close(logChannel)
				RemoveErrFile()
				RemoveOutFile()
				d("заканичиваем работу ll")
				return
			case mes = <-logChannel:
				// fmt.Printf("\t\t\tgot =%d\n", unsafe.Pointer(mes))
				sendToStreams(mes)
			}
		}
	}()
	
	return logChannel, doneChan, nil
}

func sendToStreams(mes *ee.Message) {
	lock.RLock()
	defer lock.RUnlock()
	for _, f := range funcs {
		d(currentLogLevel, "=currentLogLevel <> mes.Level()=", mes.Level())
		
		if currentLogLevel >= mes.Level() {
			f(mes)
		}
	}
}

func filter(m *ee.Message) *ee.Message {
	d(m.Level(), "=m.Level() <= currentLogLevel=", currentLogLevel)
	if m.Level() > currentLogLevel {
		return nil
	}
	return m
}

func SetFilterFunc(f FilterFunc, needLock bool) {
	if needLock {
		lock.Lock()
		defer lock.Unlock()
	}
	filterFunc = f
	
}
