package llog

import (
	"strconv"
	"testing"
	
	ee "gitlab.com/nikonor/eerrors"
)

func TestMesPool_Flush(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	
	var p1, p2 Pool
	
	lp1 := ee.NewPrefix()
	lp2 := ee.NewPrefix()
	
	for l := 3; l <= 7; l++ {
		lStr := strconv.Itoa(l)
		p1.Add(ee.Get(l * -1).WithMessage("level was=" + lStr).WithPrefix(lp1))
		p2.Add(ee.Get(l * -1).WithMessage("level was=" + lStr).WithPrefix(lp2))
	}
	
	p1.Flush()
	p2.SetLevel(ee.ERROR)
	p2.Flush()
	
}
