package llog

import (
	"fmt"
	"log/syslog"
	"os"
	
	ee "gitlab.com/nikonor/eerrors"
)

const (
	SYSLOGMAXSTRGINGLENGTH = 512
)

var (
	syslogger *syslog.Writer
)

func stdout(mes *ee.Message) {
	d("\tstdout")
	h := os.Stdout
	if mes.Level() == ee.ERROR {
		h = os.Stderr
	}
	fmt.Fprintln(h, "stdout:", mes.String())
}

func efile(mes *ee.Message) {
	d("\tefile")
	if mes.Level() == ee.ERROR {
		if errFH != nil {
			fmt.Fprintln(errFH, mes.String())
		}
	}
}

func ofile(mes *ee.Message) {
	d("\tdofile")
	if outFH != nil {
		fmt.Fprintln(outFH, mes.String())
	}
}

func writeToSysLog(mes *ee.Message) {
	m := *mes
	mm := &m
	mm = mm.WOTime()
	s := mm.String()
	if len(s) > SYSLOGMAXSTRGINGLENGTH {
		s = s[:SYSLOGMAXSTRGINGLENGTH]
	}
	
	if mes.Level() <= ee.ERROR {
		syslogger.Err(s)
	}
	
	// switch mes.Level() {
	// case ee.ERROR:
	// 	syslogger.Err(s)
	// case ee.WARNING:
	// 	syslogger.Warning(s)
	// case ee.INFO:
	// 	syslogger.Info(s)
	// default:
	// 	syslogger.Debug(s)
	// }
	
}
