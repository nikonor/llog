package llog

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"testing"
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
)

func begin() {
	ee.New(1, 500, "Тестовая ошибка::", "Ошибка", ee.ERROR)
	ee.New(2, 0, "Debug::", "", ee.DEBUG)
}

func TestMultiStart(t *testing.T) {
	DebugToggle()
	// первый старт д.пройти нормально
	_, doneCh, err := Start(ee.DEBUG)
	id := StartStdoutStream()
	fmt.Printf("id of stdout stream=%d\n", id)
	if err != nil {
		t.Error("Ошибка старта модуля логирования #1", err)
	}
	time.Sleep(time.Second)
	
	// проверка на попутку повторного запуска
	_, _, err = Start(ee.DEBUG)
	if err == nil {
		t.Error("Ошибка старта модуля логирования #2", err)
	}
	println("close doneCh")
	close(doneCh)
	time.Sleep(time.Second)
	
	// завершаем процесс и стартуем его снова. Ошибок снова быть не должно.
	_, doneCh, err = Start(ee.DEBUG)
	if err != nil {
		t.Error("Ошибка старта модуля логирования #3", err)
	}
	close(doneCh)
}

func TestFilter(t *testing.T) {
	begin()
	// DebugToggle()
	logCh, doneCh, _ := Start(ee.ERROR)
	id := StartStdoutStream()
	fmt.Printf("id of stdout stream=%d\n", id)
	
	// ошибка с abcdef должна вывестись
	m := ee.Get(1).WithPrefix("abcdef")
	logCh <- m
	// сообщение с a-a-a не должно
	d := ee.Get(2).WithMessage("а-а-а", 22)
	logCh <- d
	
	close(doneCh)
	
	time.Sleep(time.Second)
}

func TestStart(t *testing.T) {
	begin()
	// DebugToggle()
	
	logCh, doneCh, _ := Start(ee.DEBUG)
	id := StartStdoutStream()
	fmt.Printf("id of stdout stream=%d\n", id)
	m1 := ee.Get(2).WithMessage("debug#1. выводится")
	logCh <- m1
	e1 := ee.Get(1).WithAt("ll_test.go", "TestStart").WithMessage("error#2. выводит")
	logCh <- e1
	//
	SetLogLevel(ee.ERROR)
	time.Sleep(500 * time.Microsecond)
	m2 := ee.Get(2).WithMessage("debug#3. не выводится")
	logCh <- m2
	e2 := ee.Get(1).WithAt("ll_test.go", "TestStart").WithMessage("error#4. выводится")
	logCh <- e2
	
	close(doneCh)
	
	time.Sleep(time.Second)
}

func TestFile(t *testing.T) {
	begin()
	DebugToggle()
	logCh, doneCh, _ := Start(ee.DEBUG)
	StartStdoutStream()
	// StartSyslogStream()
	
	eFileName := "/tmp/ll_debug_err.txt"
	if err := AddErrFile(eFileName); err != nil {
		t.Error("Error: ", err)
	}
	// m1 := ee.Get(0).WithPrefix("Сообщение #1").WithMessage("Запись от " + time.Now().Format(time.RFC3339))
	// logCh <- m1
	// e1 := ee.Get(1).WithPrefix("Ошибка #2").WithAt("ll_test.go", "TestFile")
	// logCh <- e1
	
	oFileName := "/tmp/ll_debug_out.txt"
	if err := AddOutFile(oFileName); err != nil {
		t.Error("Error: ", err)
	}
	m2 := ee.Get(0).WithPrefix("Сообщение #3").WithMessage("Запись от " + time.Now().Format(time.RFC3339))
	logCh <- m2
	e2 := ee.Get(1).WithPrefix("Ошибка #4").WithAt("ll_test.go", "TestFile")
	logCh <- e2
	
	close(doneCh)
	time.Sleep(time.Second)
	
}

// func TestParseIn(t *testing.T) {
// 	cases := []struct {
// 		in    interface{}
// 		want  string
// 		level int
// 	}{
// 		{in: "test#1", want: "test#1", level: 7},                       // передаем строку
// 		{in: []interface{}{"test#2", 22}, want: "test#2 22", level: 7}, // передаем массив интерфесов
// 		{in: errors.New("test#3"), want: "test#3", level: 3},           // передаем error
// 		{in: nil, want: "<nil>", level: 7},                             // передаем error
// 	}
//
// 	for _, c := range cases {
// 		got := parseIn(c.in)
// 		fmt.Printf("Тест: получили=%d, хотели=%d\n\t\tполучили=%s\n\t\tхотели=%s\n", got.Level(), c.level, got.String(), c.want)
// 		if !strings.Contains(got.String(), c.want) {
// 			t.Errorf("\tОшибка текста: ждали=%s, получили=%s\n", c.want, *got)
// 		}
// 		if got.Level() != c.level {
// 			t.Errorf("\tОшибка уровня: ждали=%d, получили=%d\n", c.level, got.Level())
// 		}
// 	}
// }

func TestLog(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	// StartSyslogStream()
	ee.SetCurLogLevel(ee.DEBUG)
	
	go func() {
		Log(ee.Debug().WithMessage("старт"))
		defer func() {
			Log(ee.Debug().WithMessage("финиш"))
		}()
		Log(ee.Error().WithMessage("время=" + time.Now().Format(time.RFC3339)))
		time.Sleep(2 * time.Second)
		Log(ee.Debug().WithMessage("поспали"))
	}()
	
	time.Sleep(3 * time.Second)
}

func TestEELogLevel(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	// StartSyslogStream()
	ee.SetCurLogLevel(ee.DEBUG)
	
	logPrefix := ee.NewPrefix()
	d := ee.Debug().WithPrefix(logPrefix).WithAt("ll_test.go", "TestEELogLevel").WithMessage("It's Debug #1")
	// fmt.Printf("\t\t\td   =%d\n", unsafe.Pointer(d))
	e := ee.Error().WithPrefix(logPrefix).WithAt("ll_test.go", "TestEELogLevel").WithMessage("It's Error #1")
	Log(d)
	Log(e)
	Log(d)
	Log(e)
	
	SetLogLevel(ee.ERROR)
	ee.SetCurLogLevel(ee.ERROR)
	d = ee.Debug().WithPrefix(logPrefix).WithAt("ll_test.go", "TestEELogLevel").WithMessage("It's Debug #2---")
	e = ee.Error().WithPrefix(logPrefix).WithAt("ll_test.go", "TestEELogLevel").WithMessage("It's Error #2")
	Log(d)
	Log(e)
	time.Sleep(time.Second)
}

func TestDubleStart(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	// DebugToggle()
	
	Log(ee.Debug().WithMessage("String #1"))
	
	Stop()
	
	Start(ee.DEBUG)
	StartStdoutStream()
	Log(ee.Debug().WithMessage("String #2"))
	
	time.Sleep(time.Second)
}

func TestFlush(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	
	go func() {
		for i := 1; i <= 10; i++ {
			Log(ee.Debug().WithMessage("---->" + " @" + time.Now().Format(time.RFC3339Nano)))
			time.Sleep(2 * time.Second)
		}
	}()
	
	var a []*ee.Message
	for i := 1; i <= 10; i++ {
		eerr := ee.Debug()
		a = append(a, eerr.WithMessage("idx="+strconv.Itoa(i)+" @"+time.Now().Format(time.RFC3339Nano)))
		time.Sleep(time.Second)
	}
	
	for _, aa := range a {
		aa.SetLevel(ee.ERROR)
		Log(aa)
	}
	
}

func TestDefer(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	Log(ee.Debug().WithMessage("Start"))
	defer func() {
		Log(ee.Debug().WithMessage("Finish"))
	}()
	
	time.Sleep(time.Second)
}

func TestQueue(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	lp := ee.NewPrefix()
	Log(ee.Debug().WithMessage("Start").WithPrefix(lp))
	defer func() {
		Log(ee.Debug().WithMessage("Finish").WithPrefix(lp))
	}()
	
	eerr := ee.Debug()
	eerr = eerr.WithMessage("Test Message")
	eerr = eerr.WithAt("ll_test.go", "TestQueue").WithStringNum(247).WithPrefix(lp)
	
	Log(eerr)
	Log(ee.Debug().WithMessage("Second test Message WO Time").WOTime().WithPrefix(lp))
	
}

func TestTimeInOutFile(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	// StartSyslogStream()
	AddOutFile("/tmp/test.out")
	
	lp := ee.NewPrefix()
	Log(ee.Debug().WithMessage("Start").WithPrefix(lp))
	defer func() {
		Log(ee.Debug().WithMessage("Finish").WithPrefix(lp))
		time.Sleep(3 * time.Second)
	}()
	
	eerr := ee.Debug()
	eerr = eerr.WithMessage("Test Message")
	eerr = eerr.WithAt("ll_test.go", "TestTimeInOutFile").WithStringNum(268).WithPrefix(lp)
	
	Log(eerr)
	
}

func TestErrPreset(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	// StartSyslogStream()
	
	e, _ := ee.New(22, 222, "priv", "pub", 3)
	
	for i := 0; i < 3; i++ {
		Log(ee.Error().WithMessage("idx=", i+1, ", time=", time.Now().Format(time.RFC3339Nano)))
		Log(e.WithMessage("preSet"))
		time.Sleep(time.Second)
	}
}

func TestNewArgs(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	Log(ee.Error().WithMessage("Это тестый *ee.Message").WithPrefix(ee.NewPrefix()))
	Log(errors.New("это тестовая error"))
	
	SetLogLevel(ee.ERROR)
	Log(ee.Error().WithMessage("Это тестый *ee.Message #2").WithPrefix(ee.NewPrefix()))
	Log(errors.New("это тестовая error #2"))
	time.Sleep(time.Second)
	
}

func TestNewFilterFunc(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	exErr := errors.New("пример определенной ошибки")
	
	f := func(err error) *ee.Message {
		switch {
		case err == exErr:
			return ee.Error().WithError(err).WithMessage("Попали в exErr")
		default:
			return ee.Get(-1 * ee.DEBUG).WithMessage(err)
		}
	}
	
	SetFilterFunc(f, true)
	
	Log(ee.Error().WithMessage("Это тестый *ee.Message").WithPrefix(ee.NewPrefix()))
	Log(errors.New("это тестовая error"))
	Log(exErr)
	
	time.Sleep(time.Second)
	
}

func TestLen(t *testing.T) {
	Start(ee.DEBUG)
	StartStdoutStream()
	// exErr := ee.Error().WithMessage(ee.NewPrefix()).WithMessage("QQQQQQQ")
	
	rand.Seed(time.Now().UnixNano())
	
	for i := 0; i < 100; i++ {
		exErr := ee.Error().WithMessage(ee.NewPrefix()).WithMessage("QQQQQQQ")
		time.Sleep(time.Duration(rand.Intn(1000)))
		Log(exErr)
	}
}
