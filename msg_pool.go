package llog

import (
	"sync"
	
	ee "gitlab.com/nikonor/eerrors"
)

type Pool struct {
	sync.Mutex
	l           []*ee.Message
	changeLevel bool
	newLevel    int
}

// Add - добавление сообщения в пул
func (p *Pool) Add(m *ee.Message) {
	p.Lock()
	defer p.Unlock()
	p.l = append(p.l, m)
	
}

// SetLevel - устанавливаем уровень
func (p *Pool) SetLevel(newLevel int) {
	p.Lock()
	defer p.Unlock()
	p.changeLevel = true
	p.newLevel = newLevel
	
}

// Flush - вывод сообщений из пула
//      если level равен -1, то увроень сообщений менять не будет,
func (p Pool) Flush() {
	p.Lock()
	defer p.Unlock()
	for _, m := range p.l {
		if p.changeLevel {
			m = m.SetLevel(p.newLevel)
		}
		Log(m)
	}
}
