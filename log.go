package llog

import (
	ee "gitlab.com/nikonor/eerrors"
)

// Log - главная функция для работы с логом.
//      Получает: *ee.Message, ee.Message, error, string, []interface
//                  и выводит их в рамках своей логики
// func Log(in *ee.Message) {
func Log(err error) {
	// fmt.Printf("\t\t\tsend=%d\n", unsafe.Pointer(in))
	switch in := err.(type) {
	case *ee.Message:
		if in.Level() > currentLogLevel {
			return
		}
		logChannel <- in
	case error:
		if filterFunc != nil {
			in := filterFunc(err)
			logChannel <- in
		} else {
			if ee.DEBUG <= currentLogLevel {
				logChannel <- ee.Get(-1 * ee.DEBUG).WithMessage(err)
			}
		}
	}
}

func LogString(in string) {
	logChannel <- ee.Get(0).WithMessage(in)
}

func LogErr(err error) {
	logChannel <- ee.Get(-1 * ee.ERROR).WithMessage(err)
}

// func parseIn(in interface{}) *ee.Message {
// 	var (
// 		m *ee.Message
// 	)
// 	switch in.(type) {
// 	case ee.Message:
// 		mm := in.(ee.Message)
// 		m = &mm
// 	case *ee.Message:
// 		m = in.(*ee.Message)
// 	case error:
// 		m = ee.Get(-1 * ee.ERROR).WithMessage(in.(error).Error())
// 	case string:
// 		m = ee.Get(0).WithMessage(in.(string))
// 	default:
// 		m = ee.Get(0).WithMessage(fmt.Sprint(" ", in))
// 	}
// 	return m
// }
